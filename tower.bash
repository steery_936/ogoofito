#!/bin/bash -ex

host=nuc01.hopto.org
username=admin
password=password
ID=7

echo "Configuring Tower Settings"
hostval=$(tower-cli config host $host)
userval=$(tower-cli config username $username)
passwordval=$(tower-cli config password $password)

if [[ $userval == "username: " ]] || [[ $passwordval == "password: " ]]
then
  echo "WARNING: Configuration has not been fully set";
  echo "   You will want to run the $ tower-cli config ";
  echo "   command for host, username, and password ";
fi

echo " current configuration settings:"
echo $hostval
echo $userval
echo $passwordval

tower-cli config verify_ssl false

# Let's run a tower-cli job
tower-cli job launch --job-template $ID --monitor

